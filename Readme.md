# Release

See https://github.com/bettse/Janus/releases/0.5

Icon from http://findicons.com/icon/69400/circle_grey?id=332952

New UI design by [Bas van der Ploeg](http://basvanderploeg.nl)

# Script Ideas:
 * current stock price
 * Play Next/Prev
 * Volume up/down/certain level
 * open google earth location
 * open iphoto album
 * open facetime to someone using facetime://
 * control hue lights (movie mode?)
 * Print App icon to cardboard and tag, link to open app
 * Combination/Chain scripts (set lights + start music + open slideshow)
 * Delegate launching/scripting to something like Alfred?

# Stateful scripts
 * Last used
 * How long tag has been on reader
 * Number of scans per day

# Ideas from plannete domotique
 * http://www.planete-domotique.com/confort/nabaztag-karotz/mir-ror/mir-ror-bulk-sans-nanoz.html
 * Launch files on your computer
 * Viewing websites
 * Display videos, photos, music
 * Voice reading info from your web site (RSS)
 * Launches Podcasts from the internet
 * Launch Webradios
 * Sending emails containing variable parts
 * Sending voice messages
 * Diffusion Weather, Stocks, Air Quality
 * Counting, recording and recalling past uses of an object
 * Lock PC, launch the screen saver
 * Send Twitter messages with variable parts
 * Receiving emails and message subject
 * Sending sensor data to external sites through URL
 * Reading books (sold separately)
 * Launching and piloting iTunes
 * And launch Skype call


# sites
 * http://www.journaldulapin.com/2013/03/01/reutiliser-facilement-un-mirror-sous-mac-os-x/
 * http://www.journaldulapin.com/2013/02/12/reutiliser-un-mirror-sous-mac-os-x/
 * http://labo.lcprod.net/2013/02/application-mac-os-x-gestion-du-mirror-de-violet-le-lecteur-rfid-associe-au-nabaztags/
 * http://nabaztag.forumactif.fr/f70-mirror-mon-beau-mirror


#todo
 * gourse and git-playback
 * Janus as arbitrary event -> action framework.
 * Right now Mirror causes tagEvents, why not a clock that causes clockEvents or timer that causes Timer events.
