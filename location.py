#!/usr/bin/python

import sys
import json
from os import system
from os import path
import ConfigParser

if(len(sys.argv) < 5):
    sys.exit()

lat = sys.argv[1]
lon = sys.argv[2]
tagid = sys.argv[3]
friendlyname = sys.argv[3]
if(dir == 'OUT'):
    sys.exit()

configfile = tagid + '.cfg'

kml = """<?xml version="1.0" encoding="UTF-8"?>
<kml xmlns="http://www.opengis.net/kml/2.2" xmlns:gx="http://www.google.com/kml/ext/2.2">
    <Document id="feat_1">
        <Placemark id="feat_2">
            <name>""" + friendlyname  + """</name>
            <Point id="geom_0">
                <coordinates>""" + lon + """,""" + lat + """</coordinates>
            </Point>
        </Placemark>
    </Document>
</kml>
"""
f = open('%s.kml' % tagid, 'w')
f.write(kml)
f.close()

system("open %s.kml" % tagid)

