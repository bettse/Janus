//
//  IOHIDBlockAdapter.c
//  Janus
//
//  Created by Eric Betts on 9/1/15.
//  Copyright © 2015 Eric Betts. All rights reserved.
//

#include "IOHIDBlockAdapter.h"

void device_callback_helper(void *inBlock, IOReturn inResult, void *inSender, IOHIDDeviceRef inIOHIDDeviceRef) {
    IOHIDDeviceBlock block = (IOHIDDeviceBlock)inBlock;
    block(inResult, inSender, inIOHIDDeviceRef);
}

void input_callback_helper(void *inBlock, IOReturn result, void *sender, IOHIDReportType type, uint32_t reportID, uint8_t *report, CFIndex reportLength) {
    IOHIDReportBlock block = (IOHIDReportBlock)inBlock;
    block(result, sender, type, reportID, report, reportLength);
}
