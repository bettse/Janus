//
//  Timer.m
//  Janus
//
//  Created by Eric Betts on 6/7/13.
//  Copyright (c) 2013 Eric Betts. All rights reserved.
//

#import "Timer.h"

@implementation Timer

- (id) init {
    self = [super init];
    if (self != nil) {
        start = nil;
        end = nil;
    }
    return self;
}

-(NSArray*) inEvent
{
    [self startTimer];
    self.subs[@"seconds"] = @"";
    self.subs[@"minutes"] = @"";
    
    [self legate];

    return self.parameter;
}
-(NSArray*) outEvent
{
    [self stopTimer];
    self.subs[@"seconds"] = [NSNumber numberWithDouble:[self timeElapsedInSeconds]];
    self.subs[@"minutes"] = [NSNumber numberWithDouble:[self timeElapsedInMinutes]];

    [self legate];

    return self.parameter;
}

+(NSString*) getHelpText
{
    return @"%s for timer in seconds, %m for timer in minutes";
}

- (void) startTimer {
    start = [NSDate date];
}

- (void) stopTimer {
    end = [NSDate date];
}

- (double) timeElapsedInSeconds {
    return [end timeIntervalSinceDate:start];
}

- (double) timeElapsedInMilliseconds {
    return [self timeElapsedInSeconds] * 1000.0f;
}

- (double) timeElapsedInMinutes {
    return [self timeElapsedInSeconds] / 60.0f;
}

@end
