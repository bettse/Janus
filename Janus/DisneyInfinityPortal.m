//
//  DisneyInfinityPortal.m
//  Janus
//
//  Created by Eric Betts on 8/30/15.
//  Copyright © 2015 Eric Betts. All rights reserved.
//

#import "DisneyInfinityPortal.h"

@implementation DisneyInfinityPortal

static DisneyInfinityPortal *singleton;
static int reportSize = 0x20;
static int productId = 0x0129;
static int vendorId = 0x0e6f;

+ (DisneyInfinityPortal *)getInstance {
    if (!singleton) {
        singleton = [[self alloc] init];
    }
    return singleton;
}

- (id)init {
    self = [super init];
    if (self) {
        currentTags = [NSMutableDictionary new];
        __weak id weakSelf = self;
        matching = ^void(IOReturn inResult, void *inSender, IOHIDDeviceRef inIOHIDDeviceRef) {
            [weakSelf connected:inResult from:inSender for:inIOHIDDeviceRef];
        };

        removal = ^void(IOReturn inResult, void *inSender, IOHIDDeviceRef inIOHIDDeviceRef) {
            [weakSelf disconnected:inResult from:inSender for:inIOHIDDeviceRef];
        };

        inputReport = ^void(IOReturn result, void *sender, IOHIDReportType type, uint32_t reportID, uint8_t *report, CFIndex reportLength) {
          [weakSelf parseInput:[NSData dataWithBytes:report length:reportLength]];
        };
    }
    return self;
}

- (void)parseInput:(NSData *)input {
    //NSLog(@"Input: %@", input);
    Byte *inputBytes = (Byte *)input.bytes;
    Byte command[] = {0xFF, 0x03, 0xB4, 0x00, 0x00, 0xB6};
    NSData *uid;
    Byte platformNumber, length, direction;

    //NSLog(@"input: %@", input);
    switch (inputBytes[0]) {
        case 0xAA: //Response from tagid command
            length = inputBytes[1];
            platformNumber = inputBytes[3];
            if (length == 9) {
                uid = [input subdataWithRange:NSMakeRange(4, 7)];
                currentTags[@(platformNumber)] = uid; //Save uid for when token leaves
                [self handleTag:uid inDirection:1];   //1 represents TAG_IN in Tag.h
            }
            break;
        case 0xAB: //Unprompted status update
            platformNumber = inputBytes[4];
            direction = inputBytes[5];
            if (direction == 0) {            //incoming
                command[4] = platformNumber; //Set platform number into platform parameter
                command[5] += command[4];    //Correct checksum byte
                IOHIDDeviceSetReport(self.device, kIOHIDReportTypeOutput, reportSize, command, sizeof(command));
            } else if (direction == 1) { //outgoing
                //Get uid from dictionary
                uid = currentTags[@(platformNumber)];
                //Send event
                if (uid) {
                    [currentTags removeObjectForKey:@(platformNumber)];
                    [self handleTag:uid inDirection:2];
                }
            }
            break;
        default:
            NSLog(@"Unhandled Input: %@", input);
            break;
    }
}

- (void)handleTag:(NSData *)tagID inDirection:(int)direction {
    NSDictionary *userInfo = @{ @"tagid" : tagID.hexadecimalString,
                                @"direction" : [NSNumber numberWithInt:direction] };
    dispatch_async(dispatch_get_main_queue(), ^{
      [[NSNotificationCenter defaultCenter] postNotificationName:@"TagEvent" object:nil userInfo:userInfo];
    });
}

-(void) connected:(IOReturn)inResult from:(void*)inSender for:(IOHIDDeviceRef)device {
    self.device = device;
    uint8_t *report = calloc(1, reportSize);
    if (report) {
        //Hook up input callback
        IOHIDDeviceRegisterInputReportCallback(device, report, reportSize, input_callback_helper, (__bridge void *)inputReport);

        //Initialize
        Byte bytes[] = {0xff, 0x11, 0x80, 0x00, 0x28, 0x63, 0x29, 0x20, 0x44, 0x69, 0x73, 0x6e, 0x65, 0x79, 0x20, 0x32, 0x30, 0x31, 0x33, 0xb6};
        if (self.device) {
            IOHIDDeviceSetReport(self.device, kIOHIDReportTypeOutput, reportSize, bytes, sizeof(bytes));
        }

        dispatch_async(dispatch_get_main_queue(), ^{
          [[NSNotificationCenter defaultCenter] postNotificationName:@"deviceConnected" object:nil userInfo:@{ @"class" : NSStringFromClass([self class]) }];
        });
    }
}

-(void) disconnected:(IOReturn)inResult from:(void*)inSender for:(IOHIDDeviceRef)device {
    self.device = nil;
    dispatch_async(dispatch_get_main_queue(), ^{
      [[NSNotificationCenter defaultCenter] postNotificationName:@"deviceDisconnected" object:nil userInfo:@{ @"class" : NSStringFromClass([self class]) }];
    });
}

- (void)initUsb {
    NSDictionary *dict = @{
        @kIOHIDProductIDKey : @(productId),
         @
        kIOHIDVendorIDKey : @(vendorId)
    };

    IOHIDManagerRef managerRef = IOHIDManagerCreate(kCFAllocatorDefault, kIOHIDOptionsTypeNone);
    IOHIDManagerScheduleWithRunLoop(managerRef, CFRunLoopGetCurrent(), kCFRunLoopDefaultMode);
    IOHIDManagerOpen(managerRef, 0L);
    IOHIDManagerSetDeviceMatching(managerRef, (__bridge CFMutableDictionaryRef)dict);
    IOHIDManagerRegisterDeviceMatchingCallback(managerRef, device_callback_helper, (__bridge void *)(matching));
    IOHIDManagerRegisterDeviceRemovalCallback(managerRef, device_callback_helper, (__bridge void *)(removal));
    [[NSRunLoop currentRunLoop] run];
}

@end
