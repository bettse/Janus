//
//  DarkSky.m
//  Janus
//
//  Created by Eric Betts on 6/12/13.
//  Copyright (c) 2013 Eric Betts. All rights reserved.
//

#import "DarkSky.h"

@implementation DarkSky
- (id) init {
    self = [super init];
    if (self != nil) {
        //Other stuff
        apikey = @"ec3dca9f9390c21b168dd5ff08a8b294";
        urlFormat = @"https://api.forecast.io/forecast/%@/%@,%@";
    }
    return self;
}

-(NSArray*) inEvent
{
    NSURLResponse *resp = nil;
    NSError *error = nil;
    NSString* lat = @"45.51538";
    NSString* lon = @"-122.68273";

    if (self.parameter.count > 1) {
        lat = self.parameter[0];
        lon = self.parameter[1];
    }
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:urlFormat, apikey, lat, lon]];
    NSURLRequest *theRequest = [NSURLRequest requestWithURL:url];
    NSData *response = [NSURLConnection sendSynchronousRequest: theRequest returningResponse: &resp error: &error];
    if (error) {
        NSLog(@"Error: %@", error);
        return @[];
    }
    id jsonData = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:&error];
    if (error) {
        NSLog(@"Error: %@", error);
        return @[];
    }
    NSString *current = jsonData[@"currently"][@"summary"];
    NSString *hour = jsonData[@"minutely"][@"summary"];
    NSString *report = [NSString stringWithFormat:@"It is currently %@.  Over the next hour: %@. This forecast provided by the Dark Sky API.", current, hour];
    
    return [NSArray arrayWithObject:report];
}

+(NSString*) describeExpectedInput
{
    return @"Location format: Latitude<space>Longitude";
}

+(NSString*) getHelpText
{
    return @"";
}

@end
