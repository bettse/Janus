//
//  Music.m
//  Janus
//
//  Created by Eric Betts on 6/9/13.
//  Copyright (c) 2013 Eric Betts. All rights reserved.
//

#import "Music.h"

@implementation Music

- (id) init {
    self = [super init];
    if (self != nil) {
        //Other stuff
    }
    return self;
}

-(NSArray*) inEvent
{
    NSString* script;
    NSString* playlist = [self.parameter componentsJoinedByString:@" "];;

    if ([playlist isEqualToString:@""]){
        script = @"tell application \"iTunes\" to play";
    }else{
        script = [NSString stringWithFormat:@"tell application \"iTunes\" to play the playlist named \"%@\"", playlist];
    }
    [self runScript:script];

    return self.parameter;
}

-(NSArray*) outEvent
{
    [self runScript:@"tell application \"iTunes\" to pause"];
    return self.parameter;
}

-(void) runScript:(NSString*)script
{
    NSDictionary* error;
    NSAppleScript* playPause = [[NSAppleScript alloc] initWithSource:script];
    if(![playPause executeAndReturnError:&error]){
        NSLog(@"%@", error);
    }
}

+(NSString*) describeExpectedInput
{
    return @"Playlist or empty for all";
}

@end
