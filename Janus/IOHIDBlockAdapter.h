//
//  IOHIDBlockAdapter.h
//  Janus
//
//  Created by Eric Betts on 9/1/15.
//  Copyright © 2015 Eric Betts. All rights reserved.
//

#ifndef IOHIDBlockAdapter_c
#define IOHIDBlockAdapter_c

#include <stdio.h>

#import <IOKit/hid/IOHIDLib.h>
#import <IOKit/hid/IOHIDKeys.h>
#import <IOKit/hid/IOHIDManager.h>

typedef void (^IOHIDDeviceBlock)(IOReturn, void *, IOHIDDeviceRef);
typedef void (^IOHIDReportBlock)(IOReturn, void *, IOHIDReportType, uint32_t, uint8_t *, CFIndex);

void device_callback_helper(void *inBlock, IOReturn inResult, void *inSender, IOHIDDeviceRef inIOHIDDeviceRef);
void input_callback_helper(void *inBlock, IOReturn result, void *sender, IOHIDReportType type, uint32_t reportID, uint8_t *report, CFIndex reportLength);

#endif /* IOHIDBlockAdapter_c */
