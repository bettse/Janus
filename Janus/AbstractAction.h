//
//  AbstractAction.h
//  Janus
//
//  Created by Eric Betts on 6/14/13.
//  Copyright (c) 2013 Eric Betts. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Action.h"

@interface AbstractAction : NSObject <Action> {
    
}

@property (nonatomic, retain) NSArray *parameter;
@property (nonatomic, retain) NSMutableDictionary *subs;

- (void) legate;
@end
