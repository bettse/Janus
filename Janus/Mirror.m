//
//  Mirror.m
//  Mirror Daemon
//
//  Created by Eric Betts on 3/24/13.
//  Copyright (c) 2013 Eric Betts. All rights reserved.
//

#import "Mirror.h"

@implementation Mirror

static Mirror *singleton;
static int vendorId = 0x1da8;
static int productId = 0x1301;
static int reportSize = 64;

+ (Mirror *)getInstance {
    if (!singleton) {
        singleton = [[self alloc] init];
    }
    return singleton;
}

- (id)init {
    self = [super init];
    if (self) {
        __weak id weakSelf = self;
        matching = ^void(IOReturn inResult, void *inSender, IOHIDDeviceRef inIOHIDDeviceRef) {
            [weakSelf connected:inResult from:inSender for:inIOHIDDeviceRef];
        };

        removal = ^void(IOReturn inResult, void *inSender, IOHIDDeviceRef inIOHIDDeviceRef) {
            [weakSelf disconnected:inResult from:inSender for:inIOHIDDeviceRef];
        };

        inputReport = ^void(IOReturn result, void *sender, IOHIDReportType type, uint32_t reportID, uint8_t *report, CFIndex reportLength) {
          [weakSelf input:result from:sender of:type and:reportID contents:report andLength:reportLength];
        };
    }
    return self;
}

- (NSString *)bytesToString:(uint8_t *)bytes andLength:(CFIndex)length {
    NSMutableString *result = [NSMutableString stringWithCapacity:2 * length + length - 1];
    for (int i = 0; i < length; i++) {
        [result appendFormat:@"%02X", bytes[i]];
    }
    return [result lowercaseString]; // Result is auto-released
}

- (void)setChoreography:(NSNotification *)notification {
    NSDictionary *userInfo = notification.userInfo;
    NSNumber *state = userInfo[@"state"];
    size_t bufferSize = reportSize;
    uint8_t *outputBuffer = malloc(bufferSize);
    memset(outputBuffer, 0, bufferSize);
    //Interface
    outputBuffer[0] = 0x03;
    //CID
    outputBuffer[2] = 0;
    outputBuffer[3] = 0;
    //length
    outputBuffer[4] = 0;

    if ([state boolValue]) {
        NSLog(@"Choreography on");
        //Method
        outputBuffer[1] = 0x03;
    } else {
        NSLog(@"Choreography off");
        //Method
        outputBuffer[1] = 0x01;
    }
    IOHIDDeviceSetReport(self.mirror, kIOHIDReportTypeOutput, reportSize, outputBuffer, bufferSize);
    free(outputBuffer);
}

- (void)handleTag:(NSString *)tagID inDirection:(int)direction {
    dispatch_async(dispatch_get_main_queue(), ^{
      NSDictionary *userInfo = @{ @"tagid" : tagID,
                                  @"direction" : [NSNumber numberWithInt:direction] };
      [[NSNotificationCenter defaultCenter] postNotificationName:@"TagEvent" object:nil userInfo:userInfo];
    });
}

- (void)input:(IOReturn)result from:(void *)sender of:(IOHIDReportType)type and:(uint32_t)reportID contents:(uint8_t *)report andLength:(CFIndex)reportLength {
    // process device response buffer (report) here
    //NSData *input = [NSData dataWithBytes:report length:reportLength];
    NSString *tagID = @"";
    int interface = report[0];
    int method = report[1];
    //int correlationID = report[2] * 255 + report[3];
    int dataLength = report[4];
    uint8_t *data = report + 5;
    if (reportLength && report[0]) {
        //NSLog(@"Mirror input: %@", input);
        switch (interface) {
            case 1:
                switch (method) {
                    case UPSIDE_DOWN:
                        NSLog(@"Uh oh!  Mir:ror upside down");
                        break;
                    case RIGHTSIDE_UP:
                        NSLog(@"Phew!  Mir:ror rightside up");
                        break;
                }
                break;
            case 2:
                tagID = [NSData dataWithBytes:data length:dataLength].hexadecimalString;
                [self handleTag:tagID inDirection:method];
                break;
            default:
                break;
        }
    }
}

-(void) connected:(IOReturn)inResult from:(void*)inSender for:(IOHIDDeviceRef)device {
    self.mirror = device;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setChoreography:) name:@"setChoreography" object:nil];
    uint8_t *report = calloc(1, reportSize);
    if (report) {
        IOHIDDeviceRegisterInputReportCallback(device, report, reportSize, input_callback_helper, (__bridge void *)inputReport);

        dispatch_async(dispatch_get_main_queue(), ^{
          [[NSNotificationCenter defaultCenter] postNotificationName:@"deviceConnected" object:nil userInfo:@{ @"class" : NSStringFromClass([self class]) }];
        });

        //Default to choreo off
        dispatch_async(dispatch_get_main_queue(), ^{
          [[NSNotificationCenter defaultCenter] postNotificationName:@"setChoreography" object:nil userInfo:@{ @"state" : [NSNumber numberWithBool:NO] }];
        });
    }
}

-(void) disconnected:(IOReturn)inResult from:(void*)inSender for:(IOHIDDeviceRef)device {
    self.mirror = nil;
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"setChoreography" object:nil];
    dispatch_async(dispatch_get_main_queue(), ^{
      [[NSNotificationCenter defaultCenter] postNotificationName:@"deviceDisconnected" object:nil userInfo:@{ @"class" : NSStringFromClass([self class]) }];
    });
}

- (void)initUsb {
    NSDictionary *dict = @{ @kIOHIDProductIDKey : @(productId), @
                            kIOHIDVendorIDKey : @(vendorId) };

    IOHIDManagerRef managerRef = IOHIDManagerCreate(kCFAllocatorDefault, kIOHIDOptionsTypeNone);
    IOHIDManagerScheduleWithRunLoop(managerRef, CFRunLoopGetCurrent(), kCFRunLoopDefaultMode);
    IOHIDManagerOpen(managerRef, 0L);
    IOHIDManagerSetDeviceMatching(managerRef, (__bridge CFMutableDictionaryRef)dict);
    IOHIDManagerRegisterDeviceMatchingCallback(managerRef, device_callback_helper, (__bridge void *)(matching));
    IOHIDManagerRegisterDeviceRemovalCallback(managerRef, device_callback_helper, (__bridge void *)(removal));
    [[NSRunLoop currentRunLoop] run];
}

@end
