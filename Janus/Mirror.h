//
//  mirrord.h
//  Mirror Config
//
//  Created by Eric Betts on 3/24/13.
//  Copyright (c) 2013 Eric Betts. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <IOKit/hid/IOHIDLib.h>
#import <IOKit/hid/IOHIDKeys.h>
#import <IOKit/hid/IOHIDManager.h>
#import <stdlib.h>
#import "NSData_hexadecimal.h"
#import "IOHIDBlockAdapter.h"

#define UPSIDE_DOWN 5
#define RIGHTSIDE_UP 4

@interface Mirror : NSObject {
    IOHIDDeviceBlock matching;
    IOHIDDeviceBlock removal;
    IOHIDReportBlock inputReport;
}
@property IOHIDDeviceRef mirror;

- (void)initUsb;
- (void)setChoreography:(NSNumber *)state;

+ (Mirror *)getInstance;
@end
