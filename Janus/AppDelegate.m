//
//  AppDelegate.m
//  Janus
//
//  Created by Eric Betts on 2/24/13.
//  Copyright (c) 2013 Eric Betts. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate ()

@property (weak) IBOutlet NSWindow *window;
- (IBAction)saveAction:(id)sender;

@end

@implementation AppDelegate

#pragma mark - NSApplicationDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loadTags) name:@"loadTags" object:0];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(tagEvent:) name:@"TagEvent" object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(deviceDisconnected:) name:@"deviceDisconnected" object:0];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(deviceConnected:) name:@"deviceConnected" object:0];

    [self setup];
    [self loadTags];

    self.window.releasedWhenClosed = NO;
    NSApp.activationPolicy = NSApplicationActivationPolicyAccessory;

#ifdef DEBUG
    NSApp.activationPolicy = NSApplicationActivationPolicyRegular;
    [self.window makeKeyAndOrderFront:nil];
#endif
}

- (BOOL)applicationShouldHandleReopen:(NSApplication *)sender hasVisibleWindows:(BOOL)flag {
    self.window.isVisible = YES;
    return YES;
}

- (void)applicationWillTerminate:(NSNotification *)aNotification {
    for (NSThread *daemon in self.daemons) {
        [daemon cancel];
    }
    [self.daemons removeAllObjects];
    [self.devices removeAllObjects];
}

- (void)awakeFromNib {
    self.statusItem = [[NSStatusBar systemStatusBar] statusItemWithLength:NSVariableStatusItemLength];
    NSImage *icon = [NSApp applicationIconImage];
    icon.size = NSMakeSize(18.0, 18.0);
    self.statusItem.menu = self.statusMenu;
    self.statusItem.highlightMode = YES;
    self.statusItem.image = icon;
}

#pragma mark - private / helper methods

- (void)setup {
    [self createAppSupport];
    self.af = [ActionFactory getInstance];
    self.incomingActionSelection.delegate = self.af;
    self.outgoingActionSelection.delegate = self.af;
    self.incomingActionSelection.dataSource = self.af;
    self.outgoingActionSelection.dataSource = self.af;

    [self setupDevices];
}

- (void)setupDevices {
    //Spin off device handling into its own thread
    //If I get more of these, I should make more generic...subclassing?
    self.devices = [[NSMutableArray alloc] initWithCapacity:2];
    self.daemons = [[NSMutableArray alloc] initWithCapacity:2];

    self.devices[0] = [DisneyInfinityPortal getInstance];
    self.daemons[0] = [[NSThread alloc] initWithTarget:self.devices[0] selector:@selector(initUsb) object:nil];
    [self.daemons[0] start];

    self.devices[1] = [Mirror getInstance];
    self.daemons[1] = [[NSThread alloc] initWithTarget:self.devices[1] selector:@selector(initUsb) object:nil];
    [self.daemons[1] start];
}

- (void)createAppSupport {
    NSFileManager *filemgr = [NSFileManager defaultManager];
    NSBundle *thisBundle = [NSBundle mainBundle];
    NSString *bundle = [thisBundle infoDictionary][@"CFBundleName"];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSApplicationSupportDirectory, NSUserDomainMask, YES);
    NSString *applicationSupportDirectory = paths[0];
    NSString *path = [applicationSupportDirectory stringByAppendingPathComponent:bundle];

    //Create Application Support Directory if it doesn't exist
    if (![filemgr fileExistsAtPath:path isDirectory:NULL]) {
        if (![filemgr createDirectoryAtPath:path withIntermediateDirectories:YES attributes:nil error:NULL]) {
            NSLog(@"Error: Create folder failed %@", path);
        }
    }
}

- (IBAction)searchTags:(id)sender {
    NSString *searchTerm = self.filterTags.stringValue;

    NSManagedObjectContext *moc = [self managedObjectContext];
    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:@"Tag" inManagedObjectContext:moc];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    request.entity = entityDescription;

    if (![searchTerm isEqualToString:@""]) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(name CONTAINS[c] %@) OR (uid CONTAINS[c] %@)", searchTerm, searchTerm];
        request.predicate = predicate;
    }

    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"uid" ascending:YES];
    request.sortDescriptors = @[ sortDescriptor ];

    NSError *error;
    NSArray *array = [moc executeFetchRequest:request error:&error];
    if (array == nil) {
        // Deal with error...
        NSLog(@"CoreData error: %@", error);
        return;
    }

    tags = array;
    [self.tagTable reloadData];
}

- (void)loadTags {
    NSManagedObjectContext *moc = [self managedObjectContext];
    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:@"Tag" inManagedObjectContext:moc];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    request.entity = entityDescription;
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"uid" ascending:YES];
    request.sortDescriptors = @[ sortDescriptor ];
    NSError *error;
    NSArray *array = [moc executeFetchRequest:request error:&error];
    if (array == nil) {
        // Deal with error...
        NSLog(@"CoreData error: %@", error);
        return;
    }

    tags = array;
    [self.tagTable reloadData];
}

- (void)tagEvent:(NSNotification *)notification {
    NSDictionary *userInfo = notification.userInfo;
    NSString *uid = userInfo[@"tagid"];
    NSNumber *direction = userInfo[@"direction"];

    Tag *aTag = [Tag findByUid:uid];
    if (aTag) {
        [aTag doActionForDirection:(direction.integerValue == TAG_IN) ? @"IN" : @"OUT"];
    } else {
        //Create it
        aTag = [NSEntityDescription insertNewObjectForEntityForName:@"Tag" inManagedObjectContext:self.managedObjectContext];
        aTag.uid = uid;
        NSLog(@"New tag %@", aTag);
        [self loadTags];
        [self.window makeKeyAndOrderFront:nil];
    }
}

- (void)deviceConnected:(NSNotification *)notification {
    NSDictionary *userInfo = notification.userInfo;
    NSString *class = userInfo[@"class"];
    NSLog(@"%@ connected", class);
    self.connected.doubleValue = 1;
}

- (void)deviceDisconnected:(NSNotification *)notification {
    NSDictionary *userInfo = notification.userInfo;
    NSString *class = userInfo[@"class"];
    NSLog(@"%@ disconnected", class);
    self.connected.doubleValue = 0;
}

#pragma mark - Responses to UI events

- (IBAction)saveMenu:(id)sender {
    [self saveButton:sender];
}

- (IBAction)saveButton:(id)sender {
    if (self.tagTable.selectedRow == -1) {
        return;
    }
    Tag *thisTag = tags[self.tagTable.selectedRow];

    thisTag.name = self.tagName.stringValue;
    NSLog(@"%s %@", __PRETTY_FUNCTION__, thisTag);

    thisTag.incomingAction = self.incomingActionSelection.stringValue;
    thisTag.outgoingAction = self.outgoingActionSelection.stringValue;

    thisTag.incomingParams = self.incomingActionParameter.stringValue;
    thisTag.outgoingParams = self.outgoingActionParameter.stringValue;

    [self saveAction:sender];
    [self loadTags];
}

- (IBAction)deleteButton:(id)sender {
    if (self.tagTable.selectedRow == -1)
        return; //Guard for no tags in list
    Tag *tag = tags[self.tagTable.selectedRow];
    [self.managedObjectContext deleteObject:tag];
    [self saveAction:sender];
    [self loadTags];
}

- (IBAction)showWindow:(id)sender {
    [NSApp activateIgnoringOtherApps:YES];
    [self.window makeKeyAndOrderFront:sender];
}

- (IBAction)setChoreo:(id)sender {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"setChoreography" object:nil userInfo:@{ @"state" : [NSNumber numberWithBool:[sender state]] }];
}

#pragma mark - NSTable stuff

- (NSInteger)numberOfRowsInTableView:(NSTableView *)tableView {
    return tags.count;
}

- (NSView *)tableView:(NSTableView *)tableView viewForTableColumn:(NSTableColumn *)tableColumn row:(NSInteger)row {
    NSTableCellView *result = [tableView makeViewWithIdentifier:tableColumn.identifier owner:self];

    Tag *tag = tags[row];
    for (NSView *view in result.subviews) {
        if ([view.identifier isEqualToString:@"tagName"]) {
            ((NSTextField *)view).stringValue = tag.name;
        } else if ([view.identifier isEqualToString:@"tagId"]) {
            ((NSTextField *)view).stringValue = tag.uid;
        }
    }

    return result;
}

- (void)tableViewSelectionDidChange:(NSNotification *)aNotification {
    Tag *tag = tags[self.tagTable.selectedRow];
    self.tagName.stringValue = tag.name;
    self.tagUid.stringValue = tag.uid;

    self.incomingActionParameter.stringValue = tag.incomingParams;
    self.outgoingActionParameter.stringValue = tag.outgoingParams;

    self.incomingActionSelection.stringValue = tag.incomingAction;
    self.outgoingActionSelection.stringValue = tag.outgoingAction;
}

#pragma mark - Core Data stack

@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize managedObjectContext = _managedObjectContext;

- (NSURL *)applicationDocumentsDirectory {
    // The directory the application uses to store the Core Data store file. This code uses a directory named "org.ericbetts.Janus" in the user's Application Support directory.
    NSURL *appSupportURL = [[[NSFileManager defaultManager] URLsForDirectory:NSApplicationSupportDirectory inDomains:NSUserDomainMask] lastObject];
    return [appSupportURL URLByAppendingPathComponent:@"org.ericbetts.Janus"];
}

- (NSManagedObjectModel *)managedObjectModel {
    // The managed object model for the application. It is a fatal error for the application not to be able to find and load its model.
    if (_managedObjectModel) {
        return _managedObjectModel;
    }

    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"TagModel" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    // The persistent store coordinator for the application. This implementation creates and returns a coordinator, having added the store for the application to it. (The directory for the store is created, if necessary.)
    if (_persistentStoreCoordinator) {
        return _persistentStoreCoordinator;
    }

    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSURL *applicationDocumentsDirectory = [self applicationDocumentsDirectory];
    BOOL shouldFail = NO;
    NSError *error = nil;
    NSString *failureReason = @"There was an error creating or loading the application's saved data.";

    // Make sure the application files directory is there
    NSDictionary *properties = [applicationDocumentsDirectory resourceValuesForKeys:@[ NSURLIsDirectoryKey ] error:&error];
    if (properties) {
        if (![properties[NSURLIsDirectoryKey] boolValue]) {
            failureReason = [NSString stringWithFormat:@"Expected a folder to store application data, found a file (%@).", [applicationDocumentsDirectory path]];
            shouldFail = YES;
        }
    } else if ([error code] == NSFileReadNoSuchFileError) {
        error = nil;
        [fileManager createDirectoryAtPath:[applicationDocumentsDirectory path] withIntermediateDirectories:YES attributes:nil error:&error];
    }

    if (!shouldFail && !error) {
        NSPersistentStoreCoordinator *coordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
        NSURL *url = [applicationDocumentsDirectory URLByAppendingPathComponent:@"OSXCoreDataObjC.storedata"];
        if (![coordinator addPersistentStoreWithType:NSXMLStoreType configuration:nil URL:url options:nil error:&error]) {
            coordinator = nil;
        }
        _persistentStoreCoordinator = coordinator;
    }

    if (shouldFail || error) {
        // Report any error we got.
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        dict[NSLocalizedDescriptionKey] = @"Failed to initialize the application's saved data";
        dict[NSLocalizedFailureReasonErrorKey] = failureReason;
        if (error) {
            dict[NSUnderlyingErrorKey] = error;
        }
        error = [NSError errorWithDomain:@"YOUR_ERROR_DOMAIN" code:9999 userInfo:dict];
        [[NSApplication sharedApplication] presentError:error];
    }
    return _persistentStoreCoordinator;
}

- (NSManagedObjectContext *)managedObjectContext {
    // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.)
    if (_managedObjectContext) {
        return _managedObjectContext;
    }

    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (!coordinator) {
        return nil;
    }
    _managedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
    [_managedObjectContext setPersistentStoreCoordinator:coordinator];

    return _managedObjectContext;
}

#pragma mark - Core Data Saving and Undo support

- (IBAction)saveAction:(id)sender {
    // Performs the save action for the application, which is to send the save: message to the application's managed object context. Any encountered errors are presented to the user.
    if (![[self managedObjectContext] commitEditing]) {
        NSLog(@"%@:%@ unable to commit editing before saving", [self class], NSStringFromSelector(_cmd));
    }

    NSError *error = nil;
    if ([[self managedObjectContext] hasChanges] && ![[self managedObjectContext] save:&error]) {
        [[NSApplication sharedApplication] presentError:error];
    }
}

- (NSUndoManager *)windowWillReturnUndoManager:(NSWindow *)window {
    // Returns the NSUndoManager for the application. In this case, the manager returned is that of the managed object context for the application.
    return [[self managedObjectContext] undoManager];
}

- (NSApplicationTerminateReply)applicationShouldTerminate:(NSApplication *)sender {
    // Save changes in the application's managed object context before the application terminates.

    if (!_managedObjectContext) {
        return NSTerminateNow;
    }

    if (![[self managedObjectContext] commitEditing]) {
        NSLog(@"%@:%@ unable to commit editing to terminate", [self class], NSStringFromSelector(_cmd));
        return NSTerminateCancel;
    }

    if (![[self managedObjectContext] hasChanges]) {
        return NSTerminateNow;
    }

    NSError *error = nil;
    if (![[self managedObjectContext] save:&error]) {
        // Customize this code block to include application-specific recovery steps.
        BOOL result = [sender presentError:error];
        if (result) {
            return NSTerminateCancel;
        }

        NSString *question = NSLocalizedString(@"Could not save changes while quitting. Quit anyway?", @"Quit without saves error question message");
        NSString *info = NSLocalizedString(@"Quitting now will lose any changes you have made since the last successful save", @"Quit without saves error question info");
        NSString *quitButton = NSLocalizedString(@"Quit anyway", @"Quit anyway button title");
        NSString *cancelButton = NSLocalizedString(@"Cancel", @"Cancel button title");
        NSAlert *alert = [[NSAlert alloc] init];
        [alert setMessageText:question];
        [alert setInformativeText:info];
        [alert addButtonWithTitle:quitButton];
        [alert addButtonWithTitle:cancelButton];

        NSInteger answer = [alert runModal];

        if (answer == NSAlertFirstButtonReturn) {
            return NSTerminateCancel;
        }
    }

    return NSTerminateNow;
}

@end
