//
//  Tag.m
//  Janus
//
//  Created by Eric Betts on 9/7/15.
//  Copyright © 2015 Eric Betts. All rights reserved.
//

#import "Tag.h"

@implementation Tag

@dynamic uid;

// Insert code here to add functionality to your managed object subclass

- (void)awakeFromInsert {
    [super awakeFromInsert];
    self.uid = @"DEADBEEF";
    self.name = [Tag getDefaultName];
    self.incomingAction = @"Speak";
    self.incomingParams = @"%n";
    self.outgoingAction = @"Speak";
    self.outgoingParams = @"%n";
}

+ (NSString *)getDefaultName {
    return @"Novus";
}

- (NSString *)description {
    return [NSString stringWithFormat:@"<Tag: %@ - %@>", self.uid, self.name];
}

- (NSComparisonResult)compare:(Tag *)otherObject {
    return [self.uid compare:otherObject.uid];
}

- (NSString *)substituteSpecial:(NSString *)parameter {
    NSString *replacementString = parameter;
    NSString *quotedName = [NSString stringWithFormat:@"'%@'", self.name];
    replacementString = [replacementString stringByReplacingOccurrencesOfString:@"%n" withString:quotedName];
    replacementString = [replacementString stringByReplacingOccurrencesOfString:@"%i" withString:self.uid];
    replacementString = [replacementString stringByReplacingOccurrencesOfString:@"%d" withString:@"'%d'"];
    return replacementString;
}

- (BOOL)doActionForDirection:(NSString *)direction {
    ActionFactory *af = [ActionFactory getInstance];
    id<Action> action;
    if ([direction isEqualToString:@"IN"]) {
        action = [af getActionForName:self.incomingAction];
        NSArray *params = [[self substituteSpecial:self.incomingParams] componentsSeparatedByString:@" "];
        action.parameter = params;
        NSLog(@"Calling %@ %@ %@", action, direction, [params componentsJoinedByString:@","]);
        [action inEvent];
    } else if ([direction isEqualToString:@"OUT"]) {
        action = [af getActionForName:self.outgoingAction];
        NSArray *params = [[self substituteSpecial:self.outgoingParams] componentsSeparatedByString:@" "];
        action.parameter = params;
        NSLog(@"Calling %@ %@ %@", action, direction, [params componentsJoinedByString:@","]);
        [action outEvent];
    }

    return NO;
}

+ (Tag *)findByUid:(NSString *)uid {
    NSManagedObjectContext *moc = [(AppDelegate*)[[NSApplication sharedApplication] delegate] managedObjectContext];
    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:[self description] inManagedObjectContext:moc];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    request.entity = entityDescription;

    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(uid = %@)", uid];
    request.predicate = predicate;

    NSError *error;
    NSArray *array = [moc executeFetchRequest:request error:&error];
    if (array == nil) {
        // Deal with error...
        NSLog(@"Error in %s.  %@", __PRETTY_FUNCTION__, error);
        return nil;
    }
    if (array.count == 0) {
        return nil;
    }
    return array[0];
}

@end
