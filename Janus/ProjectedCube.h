//
//  ProjectedCube.h
//  Janus
//
//  Created by Eric Betts on 9/5/15.
//  Copyright © 2015 Eric Betts. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <QuartzCore/QuartzCore.h>

@interface ProjectedCube : NSView {
    NSInteger size;
}

@end
