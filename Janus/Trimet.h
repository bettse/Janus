//
//  Trimet.h
//  Janus
//
//  Created by Eric Betts on 6/12/13.
//  Copyright (c) 2013 Eric Betts. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AbstractAction.h"
#import "ISO8601DateFormatter.h"

@interface Trimet : AbstractAction <Action> {
    NSString* urlFormat;
    NSString* apikey;
    NSRange timeWindow;
}

@end
