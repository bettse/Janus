//
//  Trimet.m
//  Janus
//
//  Created by Eric Betts on 6/12/13.
//  Copyright (c) 2013 Eric Betts. All rights reserved.
//

#import "Trimet.h"

@implementation Trimet
- (id) init {
    self = [super init];
    if (self != nil) {
        //Other stuff
        apikey = @"D5A53696BCE071D40BCC482EE";
        urlFormat = @"http://developer.trimet.org/ws/V1/arrivals/locIDs/%@/appID/%@/json/true";
        timeWindow = NSMakeRange(1, 29);
    }
    return self;
}

-(NSArray*) inEvent
{
    NSString* stopid;
    NSMutableArray *myBus;
    NSURLResponse *resp = nil;
    NSError *error = nil;
    ISO8601DateFormatter *formatter = [[ISO8601DateFormatter alloc] init];

    if (self.parameter.count > 0) {
        stopid = self.parameter[0];
    }else{
        stopid = @"7594";
    }
    
    NSURLRequest *theRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:urlFormat, stopid, apikey]]];
    NSData *response = [NSURLConnection sendSynchronousRequest: theRequest returningResponse: &resp error: &error];
    id jsonData = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:&error];
    NSDictionary *resultSet = jsonData[@"resultSet"];
    NSArray *arrivals = resultSet[@"arrival"];
    myBus = [@[] mutableCopy];
    
    for(NSDictionary *bus in arrivals){
        NSArray *allKeys = [bus allKeys];
        if ([allKeys containsObject:@"route"] && [allKeys containsObject:@"estimated"]) {
            NSDate *estimated = [formatter dateFromString:bus[@"estimated"]];
            NSInteger minutes = [estimated timeIntervalSinceNow] / 60;
            if (NSLocationInRange(minutes, timeWindow)) {
                myBus[myBus.count] = [NSString stringWithFormat:@"%@ will arrive in %ld minutes", bus[@"route"], (long)minutes];
            }
        }
    }
    return myBus;
}

+(NSString*) describeExpectedInput
{
    return @"StopId";
}

@end
