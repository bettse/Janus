//
//  ActionFactory.h
//  Janus
//
//  Created by Eric Betts on 6/8/13.
//  Copyright (c) 2013 Eric Betts. All rights reserved.
//

#import <objc/runtime.h>
#import <Foundation/Foundation.h>
#import "Action.h"

@interface ActionFactory : NSObject <NSComboBoxDataSource, NSComboBoxDelegate> {
    NSString *defaultAction;
}

@property (retain) IBOutlet NSTokenField *actions;

+ (ActionFactory *)getInstance;
- (NSMutableArray *)getActions;
- (id<Action>)getActionForName:(NSString *)name;

@end
