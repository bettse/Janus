//
//  AppDelegate.h
//  Janus
//
//  Created by Eric Betts on 2/24/13.
//  Copyright (c) 2013 Eric Betts. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "DisneyInfinityPortal.h"
#import "Mirror.h"
#import "Tag.h"
#import "ActionFactory.h"

@interface AppDelegate <NSTableViewDelegate, NSTableViewDataSource> : NSObject <NSApplicationDelegate> {
    NSArray *tags;
}

#pragma mark - Core Data
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;

#pragma mark - UI Elements
@property (retain) IBOutlet NSTextField *tagName;
@property (retain) IBOutlet NSTextField *tagUid;
@property (retain) IBOutlet NSMenuItem *quitMenu;

@property (retain) IBOutlet NSTextField *actionLabel;
@property (retain) IBOutlet NSTextField *helpText;

@property (retain) IBOutlet NSComboBox *incomingActionSelection;
@property (retain) IBOutlet NSComboBox *outgoingActionSelection;

@property (retain) IBOutlet NSTextField *incomingActionParameter;
@property (retain) IBOutlet NSTextField *outgoingActionParameter;

@property (retain) IBOutlet NSTableView *tagTable;

@property (retain) IBOutlet NSSearchField *filterTags;

////Bindings
//Actions
@property IBOutlet NSLevelIndicator *connected;

//Status Bar
@property IBOutlet NSMenu *statusMenu;
@property NSStatusItem *statusItem;

//Daemons
@property (retain) NSMutableArray *devices;
@property (retain) NSMutableArray *daemons;

@property (retain) ActionFactory *af;

@end
