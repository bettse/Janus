//https://gist.github.com/hlung/6333269

/** Converts NSData to a hexadecimal string. */
@interface NSData (NSData_hexadecimalString)
/** Changes NSData object to a hex string.
 @returns hexadecimal string of NSData. Empty string if data is empty.*/
- (NSString *)hexadecimalString;
@end


@implementation NSData (NSData_hexadecimalString)

- (NSString *)hexadecimalString {
    const unsigned char *dataBuffer = (const unsigned char *)[self bytes];
    if (!dataBuffer) return [NSString string];
    
    NSUInteger          dataLength  = [self length];
    NSMutableString     *hexString  = [NSMutableString stringWithCapacity:(dataLength * 2)];
    
    for (int i = 0; i < dataLength; ++i)
        [hexString appendString:[NSString stringWithFormat:@"%02lx", (unsigned long)dataBuffer[i]]];
    
    return [NSString stringWithString:hexString];
}

@end
