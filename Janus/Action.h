//
//  Action.h
//  Janus
//
//  Created by Eric Betts on 6/8/13.
//  Copyright (c) 2013 Eric Betts. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol Action <NSObject>

@property (nonatomic, retain) NSArray *parameter;

-(NSArray*) inEvent;
-(NSArray*) outEvent;

+(NSString*) describeExpectedInput;
-(NSString*) describeExpectedInput;
+(NSString*) getHelpText;
-(NSString*) getHelpText;
+(instancetype) getInstance;

@end
