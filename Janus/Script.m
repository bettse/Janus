//
//  Script.m
//  Janus
//
//  Created by Eric Betts on 6/11/13.
//  Copyright (c) 2013 Eric Betts. All rights reserved.
//

#import "Script.h"


@implementation Script

- (id) init {
    self = [super init];
    if (self != nil) {
        //Other stuff
    }
    return self;
}

-(NSArray*) inEvent
{
    [self runScriptForDirection:@"IN"];
    return self.parameter;
}

-(NSArray*) outEvent
{
    [self runScriptForDirection:@"OUT"];
    return self.parameter;
}

- (void) runScriptForDirection:(NSString*)direction
{
    NSTask *task = [NSTask new];
    NSFileManager *filemgr = [NSFileManager defaultManager];
    NSRange theRange = {1, self.parameter.count - 1};
    NSString *script = self.parameter[0];
    NSMutableArray *arguments = [@[] mutableCopy];

    script = [script stringByReplacingOccurrencesOfString:@"file://localhost" withString:@""];
    script = [script stringByReplacingOccurrencesOfString:@"file://" withString:@""];
    script = [script stringByReplacingOccurrencesOfString:@"%20" withString:@" "];

    for (NSString *word in [self.parameter subarrayWithRange:theRange]) {
        arguments[arguments.count] = [word stringByReplacingOccurrencesOfString:@"'%d'" withString:direction];
    }

    //Confirm script exists
    if ([filemgr fileExistsAtPath: script] == NO) {
        NSLog(@"Coun't find %@", script);
        return;
    }

    //NSLog(@"Running %@ with %@", script, arguments);

    [task setLaunchPath:script];
    [task setArguments:arguments];
    //[task setStandardInput:[NSPipe pipe]];
    //[task setStandardOutput:[NSPipe pipe]];

    [task launch];
    [task waitUntilExit];

}

+(NSString*) describeExpectedInput
{
    return @"Script to run + parameters";
}

+(NSString*) getHelpText
{
    return @"%d for direction";
}

@end
