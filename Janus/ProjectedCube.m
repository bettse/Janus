//
//  ProjectedCube.m
//  Janus
//
//  Created by Eric Betts on 9/5/15.
//  Copyright © 2015 Eric Betts. All rights reserved.
//

#import "ProjectedCube.h"

@implementation ProjectedCube

- (void)drawRect:(NSRect)dirtyRect {
    [super drawRect:dirtyRect];

    // Drawing code here.
}

- (id)initWithFrame:(NSRect)frameRect {
    size = 25;

    if ((self = [super initWithFrame:frameRect])) {
        self.layer = [CALayer new];
        self.wantsLayer = YES; // the order of setLayer and setWantsLayer is crucial!

        //set up the perspective transform
        CATransform3D pt = CATransform3DIdentity;
        pt.m34 = -1.0 / 500.0;
        self.layer.sublayerTransform = pt;

        //set up the transform for cube 2 and add it
        CATransform3D c2t = CATransform3DIdentity;

        c2t = CATransform3DTranslate(c2t, size / 2, 0, 0);
        c2t = CATransform3DRotate(c2t, -M_PI, 1, 0, 0);
        //c2t = CATransform3DRotate(c2t, -M_PI, 0, 1, 0);

        CALayer *cube2 = [self cubeWithTransform:c2t];
        [self.layer addSublayer:cube2];
    }
    return self;
}

- (CALayer *)cubeWithTransform:(CATransform3D)transform {
    //create cube layer
    CATransformLayer *cube = [CATransformLayer layer];

    //add cube face 1
    CATransform3D ct = CATransform3DMakeTranslation(0, 0, size);
    [cube addSublayer:[self faceWithTransform:ct and:[NSColor blueColor]]];

    //add cube face 2
    ct = CATransform3DMakeTranslation(size, 0, 0);
    ct = CATransform3DRotate(ct, M_PI_2, 0, 1, 0);
    [cube addSublayer:[self faceWithTransform:ct and:[NSColor greenColor]]];

    //add cube face 3
    ct = CATransform3DMakeTranslation(0, -size, 0);
    ct = CATransform3DRotate(ct, M_PI_2, 1, 0, 0);
    [cube addSublayer:[self faceWithTransform:ct and:[NSColor redColor]]];

    //add cube face 4
    ct = CATransform3DMakeTranslation(0, size, 0);
    ct = CATransform3DRotate(ct, -M_PI_2, 1, 0, 0);
    [cube addSublayer:[self faceWithTransform:ct and:[NSColor brownColor]]];

    //add cube face 5
    ct = CATransform3DMakeTranslation(-size, 0, 0);
    ct = CATransform3DRotate(ct, -M_PI_2, 0, 1, 0);
    [cube addSublayer:[self faceWithTransform:ct and:[NSColor yellowColor]]];

    //add cube face 6
    ct = CATransform3DMakeTranslation(0, 0, -size);
    ct = CATransform3DRotate(ct, M_PI, 0, 1, 0);
    [cube addSublayer:[self faceWithTransform:ct and:[NSColor magentaColor]]];

    //center the cube layer within the container
    CGSize containerSize = self.bounds.size;
    cube.position = CGPointMake(containerSize.width / 2.0, containerSize.height / 2.0);

    //apply the transform and return
    cube.transform = transform;
    return cube;
}

- (CALayer *)faceWithTransform:(CATransform3D)transform and:(NSColor *)color {
    //create cube face layer
    CALayer *face = [CALayer layer];
    face.frame = CGRectMake(-size, -size, 2 * size, 2 * size);

    face.backgroundColor = color.CGColor;

    //apply the transform and return
    face.transform = transform;
    return face;
}

@end
