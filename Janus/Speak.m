//
//  Speak.m
//  Janus
//
//  Created by Eric Betts on 6/9/13.
//  Copyright (c) 2013 Eric Betts. All rights reserved.
//

#import "Speak.h"

@implementation Speak

- (id) init {
    self = [super init];
    if (self != nil) {
        //Other stuff
    }
    return self;
}


-(NSArray*) inEvent
{
    NSSpeechSynthesizer *synth = [[NSSpeechSynthesizer alloc] init];
    [synth startSpeakingString:[self.parameter componentsJoinedByString:@" "]];
    return self.parameter;
}

+(NSString*) describeExpectedInput
{
    return @"Text to speak";
}

@end
