//
//  ActionFactory.m
//  Janus
//
//  Created by Eric Betts on 6/8/13.
//  Copyright (c) 2013 Eric Betts. All rights reserved.
//

#import "ActionFactory.h"

static ActionFactory *singleton;

@implementation ActionFactory

- (id)init {
    self = [super init];
    if (self) {
        defaultAction = @"Speak";
    }
    return self;
}

+ (ActionFactory *)getInstance {
    if (!singleton) {
        singleton = [[self alloc] init];
    }
    return singleton;
}

- (NSArray *)getActions {
    NSMutableArray *actions = [@[] mutableCopy];
    int numClasses;
    Class *classes = NULL;

    classes = NULL;
    numClasses = objc_getClassList(NULL, 0);

    if (numClasses > 0) {
        classes = (__unsafe_unretained Class *)malloc(sizeof(Class) * numClasses);
        numClasses = objc_getClassList(classes, numClasses);
        for (int i = 0; i < numClasses; i++) {
            Class nextClass = classes[i];
            if (class_conformsToProtocol(nextClass, @protocol(Action))) {
                actions[actions.count] = NSStringFromClass(nextClass);
            }
        }
        free(classes);
    }
    [actions removeObject:@"AbstractAction"];
    [actions sortUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    return actions;
}

- (id<Action>)getActionForName:(NSString *)name {
    Class<Action> class = NSClassFromString(defaultAction);
    id<Action> instance = [class getInstance];

    if (NSClassFromString(name) && class_conformsToProtocol(NSClassFromString(name), @protocol(Action))) {
        class = NSClassFromString(name);
        instance = [class getInstance];
    }

    return instance;
}

#pragma mark NSCombobox

- (NSInteger)numberOfItemsInComboBox:(NSComboBox *)aComboBox {
    NSArray *actions = [self getActions];
    return actions.count;
}

- (id)comboBox:(NSComboBox *)aComboBox objectValueForItemAtIndex:(NSInteger)index {
    NSArray *actions = [self getActions];
    return actions[index];
}

@end
