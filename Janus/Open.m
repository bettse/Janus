//
//  Open.m
//  Janus
//
//  Created by Eric Betts on 6/9/13.
//  Copyright (c) 2013 Eric Betts. All rights reserved.
//

#import "Open.h"

@implementation Open
- (id) init {
    self = [super init];
    if (self != nil) {
        //Other stuff
    }
    return self;
}

-(NSArray*) inEvent
{
    if (self.parameter.count > 0) {
        NSURL *url = [NSURL URLWithString:self.parameter[0]];
        [[NSWorkspace sharedWorkspace] openURL:url];
    }
    return self.parameter;
}


+(NSString*) describeExpectedInput
{
    return @"File/app/url to open";
}


@end
