//
//  Tag.h
//  Janus
//
//  Created by Eric Betts on 9/7/15.
//  Copyright © 2015 Eric Betts. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "AppDelegate.h"
#import "Action.h"
#import "ActionFactory.h"


#define TAG_IN 1
#define TAG_OUT 2

NS_ASSUME_NONNULL_BEGIN

@interface Tag : NSManagedObject

// Insert code here to declare functionality of your managed object subclass
@property (nonatomic, retain) NSString *uid;

//- (id) initWithTagId:(NSString*)tagid;
- (NSString *)description;
- (BOOL)doActionForDirection:(NSString *)direction;

+ (NSString *)getDefaultName;
+ (Tag *)findByUid:(NSString *)uid;

@end

NS_ASSUME_NONNULL_END

#import "Tag+CoreDataProperties.h"
