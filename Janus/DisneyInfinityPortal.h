//
//  DisneyInfinityPortal.h
//  Janus
//
//  Created by Eric Betts on 8/30/15.
//  Copyright © 2015 Eric Betts. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <IOKit/hid/IOHIDLib.h>
#import <IOKit/hid/IOHIDKeys.h>
#import <IOKit/hid/IOHIDManager.h>
#import <stdlib.h>
#import "NSData_hexadecimal.h"
#import "IOHIDBlockAdapter.h"

@interface DisneyInfinityPortal : NSObject {
    IOHIDDeviceBlock matching;
    IOHIDDeviceBlock removal;
    IOHIDReportBlock inputReport;
    NSMutableDictionary *currentTags;
}

@property IOHIDDeviceRef device;

- (void)initUsb;
+ (DisneyInfinityPortal *)getInstance;

@end
