//
//  Count.m
//  Janus
//
//  Created by Eric Betts on 6/9/13.
//  Copyright (c) 2013 Eric Betts. All rights reserved.
//

#import "Count.h"

@implementation Count

- (id) init {
    self = [super init];
    if (self != nil) {
        count = 0;
        last_update = [NSDate date];
    }
    return self;
}

-(NSArray*) inEvent
{
    NSDate *midnight = [self getMidnight];
    
    if (last_update > midnight) {
        count++;
    }else{
        count = 1;
    }
    last_update = [NSDate date];
    
    self.subs[@"count"] = [NSNumber numberWithLong:count];
    self.subs[@"last_update"] = last_update;
    
    [self legate];
    
    return self.parameter;
}

-(NSDate*) getMidnight
{
    NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
    NSUInteger preservedComponents = (NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit);
    return [calendar dateFromComponents:[calendar components:preservedComponents fromDate:[NSDate date]]];
}


@end
