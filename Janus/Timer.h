//
//  Timer.h
//  Janus
//
//  Created by Eric Betts on 6/7/13.
//  Copyright (c) 2013 Eric Betts. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AbstractAction.h"

@interface Timer : AbstractAction <Action> {
    NSDate *start;
    NSDate *end;
}

- (void) startTimer;
- (void) stopTimer;
- (double) timeElapsedInSeconds;
- (double) timeElapsedInMilliseconds;
- (double) timeElapsedInMinutes;

@end
