//
//  Tag+CoreDataProperties.h
//  Janus
//
//  Created by Eric Betts on 9/7/15.
//  Copyright © 2015 Eric Betts. All rights reserved.
//
//  Delete this file and regenerate it using "Create NSManagedObject Subclass…"
//  to keep your implementation up to date with your model.
//

#import "Tag.h"

NS_ASSUME_NONNULL_BEGIN

@interface Tag (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *uid;
@property (nullable, nonatomic, retain) NSString *name;
@property (nullable, nonatomic, retain) NSString *incomingAction;
@property (nullable, nonatomic, retain) NSString *outgoingAction;
@property (nullable, nonatomic, retain) NSString *incomingParams;
@property (nullable, nonatomic, retain) NSString *outgoingParams;

@end

NS_ASSUME_NONNULL_END
