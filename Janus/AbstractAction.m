//
//  AbstractAction.m
//  Janus
//
//  Created by Eric Betts on 6/14/13.
//  Copyright (c) 2013 Eric Betts. All rights reserved.
//

#import "AbstractAction.h"


@implementation AbstractAction

- (id) init {
    if ([self class] == [AbstractAction class]) {
        @throw [NSException
                exceptionWithName:NSInternalInconsistencyException
                reason:[NSString stringWithFormat:@"Error, attempting to instantiate %@ directly.", [self class]]
                userInfo:nil];
    }
    self = [super init];
    if (self != nil) {
        //Other stuff
        self.subs = [@{} mutableCopy];
    }
    return self;
}

+ (instancetype) getInstance {
    //NSLog(@"%s %@", __PRETTY_FUNCTION__, self);
    return [[self alloc] init];
}

- (void) legate
{
    NSMutableArray *modified = [@[] mutableCopy];
    for (NSString __strong *word in self.parameter) {
        for (NSString* key in [self.subs allKeys]) {
            NSString *occurance = [NSString stringWithFormat:@"{%@.%@}", [self description], key];
            NSString *replacement = [NSString stringWithFormat:@"%@", self.subs[key]];
            word = [word stringByReplacingOccurrencesOfString:occurance withString:replacement];
        }
        modified[modified.count] = word;
    }
    self.parameter = modified;
}

-(NSArray*) inEvent
{
    return self.parameter;
}

-(NSArray*) outEvent
{
    return self.parameter;
}

- (NSString *)description
{
    return NSStringFromClass([self class]);
}

-(NSString*) describeExpectedInput
{
    return [[self class] describeExpectedInput];
}

+(NSString*) describeExpectedInput
{
    return @"";
}

-(NSString*) getHelpText
{
    return [[self class] getHelpText];
}

+(NSString*) getHelpText
{
    return @"";
}

@end
