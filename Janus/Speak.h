//
//  Speak.h
//  Janus
//
//  Created by Eric Betts on 6/9/13.
//  Copyright (c) 2013 Eric Betts. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AbstractAction.h"

@interface Speak : AbstractAction <Action> {

}

@end
