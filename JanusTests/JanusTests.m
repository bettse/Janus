//
//  JanusTests.m
//  JanusTests
//
//  Created by Eric Betts on 6/7/13.
//  Copyright (c) 2013 Eric Betts. All rights reserved.
//

#import "JanusTests.h"

@implementation JanusTests

- (void)setUp
{
    [super setUp];
    
    // Set-up code here.
}

- (void)tearDown
{
    // Tear-down code here.
    
    [super tearDown];
}

- (void)testTagFactoryIsSingleton
{
    TagFactory* tf = [TagFactory getInstance];
    XCTAssertNotNil(tf, @"Couldn't get instance of TagFactory");
    TagFactory* tf2 = [TagFactory getInstance];
    XCTAssertEqual(tf, tf2, @"Unequal TagFactory instances");
}

@end
