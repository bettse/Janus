//
//  TagTests.m
//  Janus
//
//  Created by Eric Betts on 6/7/13.
//  Copyright (c) 2013 Eric Betts. All rights reserved.
//

#import "TagTests.h"

@implementation TagTests

- (void)setUp
{
    [super setUp];
    
    // Set-up code here.
}

- (void)tearDown
{
    // Tear-down code here.
    
    [super tearDown];
}

- (void)testTagCreation
{
    Tag* aTag = [TagFactory getTagByID:@"fakeid"];
    
    XCTAssertNotNil(aTag, @"Could not create test subject.");
    XCTAssertTrue(aTag.new, @"Tag isn't marked as new");
}

- (void)testTagName
{
    //Setup
    Tag* aTag = [TagFactory getTagByID:@"fakeid"];
    XCTAssertEqualObjects(aTag.name, [Tag getDefaultName], @"Tag didn't have default name");
    NSString *givenName = @"FakeName";
    
    //Trigger
    aTag.name = givenName;
    
    //Verify
    XCTAssertNotNil(aTag, @"Could not create test subject.");
    XCTAssertEqualObjects(aTag.name, givenName, @"Tag name doesn't match");
}

- (void)testTagsArentDuplicated
{
    //Setup
    Tag* aTag = [TagFactory getTagByID:@"fakeid"];
    
    //Trigger
    Tag* bTag = [TagFactory getTagByID:@"fakeid"];
    
    //Verify
    XCTAssertEqualObjects(aTag, bTag, @"Tags with the same tagid aren't equal");
    XCTAssertEqual(aTag, bTag, @"Tags with the same tagid aren't equal");

}

@end
